#
# Provides a `js_project` wrapper script that configures:
#    - :compile         => compiles all the javascript sources
#    - :main            => creates a (hopefully!) npm-compatible library of `:compile`
#    - :esbuild         => (optionally) creates an `esbuild` bundle of the sources
#    - :npm             => (optionally) runs `npm pack` on the target
#    - :compile-tests   => (optionally, if test sources are specified) compiles all the javascript test sources
#    - :tests           => (optionally, if test sources are specified) exports the compile test sources
#    - :jest            => (optionally, if test sources are specified) runs the tests via `jest`
#

load("@build_bazel_rules_nodejs//:index.bzl", _js_library = "js_library", _pkg_npm = "pkg_npm")
load("@build_npm//@bazel/esbuild:index.bzl", _esbuild = "esbuild")
load("@build_npm//jest-cli:index.bzl", _jest_test = "jest_test")

def js_project(
        name = None,
        org = None,
        srcs = ["src/*.js"],
        tests = ["test/*.js"],
        deps = [],
        test_deps = [],
        package_json = "package.json",
        jest_config = "jest.config.js"):
    main_srcs = native.glob(srcs)
    test_srcs = native.glob(tests)

    # Export the package.json file
    native.exports_files([package_json])

    _esbuild(
        name = "compile",`
    )

    if name != None:
        # Todo: validate that the org doesn't contain a `/`?
        package_name = ("@" + org + "/" if org != None else "") + name

        lib_target = ":compile"

        _js_library(
            name = "main",
            package_name = package_name,
            srcs = [":package.json"],
            deps = [lib_target],
        )

        _pkg_npm(
            name = "npm",
            deps = [":main"],
            srcs = [":package.json"],
            package_name = package_name,
            substitutions = {"0.0.0-WORKSPACE": "1.0.0"},
        )

    if len(test_srcs) > 0:
        test_libraries = ["@npm//jest"]

        _js_project(
            name = "compile-tests",
            srcs = main_srcs + test_srcs,
            deps = test_libraries + deps + test_deps,
            out_dir = "tests",
            testonly = True,
        )

        native.filegroup(
            name = "tests",
            srcs = [":compile-tests"],
            testonly = True,
        )

        _jest_test(
            name = "jest",
            data = [jest_config] + [":tests"] + deps + test_libraries,
            args = ["--config", "".join(native.glob([jest_config])), "--runTestsByPath", "$(locations :tests)"],
            testonly = True,
        )
