#
# Provides a `python_project` wrapper script that configures:
#    - :main  => exports the main sources
#    - :tests => exports the test sources
#
load(
    "@rules_python//python:defs.bzl",
    _py_library="py_library",
    _py_binary="py_binary",
    _py_test="py_test",
)
load(
    "@rules_python//python:pip.bzl",
    _compile_pip_requirements="compile_pip_requirements",
)

def python_project(
        name,
        srcs = ["lib/**/*.py"],
        tests = ["test/**/*.py"],
        requirements = ["*_lock.txt"],
        deps = [],
        test_deps = [],
        test_entry_point = None,
):
    main_srcs = native.glob(srcs)
    test_srcs = native.glob(tests)
    requirement_srcs = native.glob(requirements) if requirements != None else []

    _py_library(
        name = "main",
        srcs = main_srcs,
        deps = deps,
        imports = ['lib'],
    )

    if len(test_srcs) > 0 and test_entry_point != None:
        _py_test(
            name = "tests",
            srcs = test_srcs,
            main = test_entry_point,
            deps = [ ':main' ] + test_deps,
            size = 'small',
        )

    # This rule adds a convenient way to update the requirements file.
    if len(requirement_srcs) > 0:
        _compile_pip_requirements(
            name = "requirements",
            extra_args = ["--allow-unsafe"],
            requirements_in = "requirements.txt",
            requirements_txt = "requirements_lock.txt",
        )