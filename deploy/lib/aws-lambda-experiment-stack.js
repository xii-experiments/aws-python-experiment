import * as cdk from '@aws-cdk/core';
import * as lambda from '@aws-cdk/aws-lambda';
import * as apigw from '@aws-cdk/aws-apigateway';

class AwsLambdaExperimentStack extends cdk.Stack {
    constructor(scope, id, props) {
        super(scope, id, props);

        const hello = new lambda.Function(this, 'HelloHandler', {
            runtime: lambda.Runtime.PYTHON_3_9,
            code: lambda.Code.fromAsset('../node_modules/@anomaly-xii/b'),
            handler: 'src/index.handler'
        });

        new apigw.LambdaRestApi(this, 'HelloEndpoint', {
            handler: hello
        });
    }
}

module.exports = { AwsLambdaExperimentStack }