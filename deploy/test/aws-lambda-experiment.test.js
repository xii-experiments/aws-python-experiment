import * as cdk from '@aws-cdk/core';
import * as AwsLambdaExperiment from '../lib/aws-lambda-experiment-stack';
import '@aws-cdk/assert/jest';


test('should create a lambda', () => {
    const app = new cdk.App();

    const stack = new AwsLambdaExperiment.AwsLambdaExperimentStack(app, 'MyTestStack');

    expect(stack)
        .toHaveResource("AWS::Lambda::Function", {
            "Handler": "src/index.handler",
            "Runtime": "nodejs14.x"
        });
});

test('should create an APIGateway endpoint', () => {
    const app = new cdk.App();

    const stack = new AwsLambdaExperiment.AwsLambdaExperimentStack(app, 'MyTestStack');

    expect(stack)
        .toHaveResource("AWS::ApiGateway::RestApi", {
            "Name": "HelloEndpoint"
        });
});
