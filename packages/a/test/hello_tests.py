import unittest
from hello import HelloWorld


class TestStringMethods(unittest.TestCase):

    def test_get_hello_should_return_correct_greeting(self):
        self.assertEqual(HelloWorld().get_hello(), 'Hello World')


if __name__ == '__main__':
    unittest.main()
