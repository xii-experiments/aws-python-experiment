import unittest

from handler import Handler


class TestStringMethods(unittest.TestCase):

    def test_process_should_return_success(self):
        self.assertEqual(
            Handler().process(None, None),
            {'statusCode': 200, 'icon': '😄', 'body': 'Greetings: Hello World'}
        )


if __name__ == '__main__':
    unittest.main()
