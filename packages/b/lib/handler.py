#
# This is a test
#
from typing import Any

import emojis
from hello import HelloWorld


class Handler:
    def __init__(self):
        self.hello_world = HelloWorld()

    def process(self, event: dict, context: Any) -> dict:
        return {
            'statusCode': 200,
            'icon': emojis.encode(":smile:"),
            'body': f"Greetings: {self.hello_world.get_hello()}"
        }
