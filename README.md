# AWS Lambda / Python Monorepo Example

This project is designed as an example of how to use Bazel to build a Python monorepo.
Specifically, this project is designed to demonstrate how these tools can be used to deploy an AWS lambda function.

## Key points

1. The project uses the [Bazel Python rules](https://github.com/bazelbuild/rules_python/), wrapped in a macro:
    1. `//:build/defs/python.bzl` provides the `python_project` macro to automatically configure build and test actions;

2. The [AWS CDK](https://aws.amazon.com/cdk/) is used for deployment of the AWS Lambda function:
    1. ... actually, this isn't done yet, but hopefully it will be soon!

3. Thanks to Bazel, the project is _almost_ entirely self-contained:
    1. In theory, the `pip` cache could be bundled alongside the project to remove remote `pip` fetches...
    2. ... however, I haven't actually bothered to work out the best way to do that yet.

## Todo

1. Actually get the CDK integrated?
2. Use `pip` to fetch a `whl` to demonstrate how that works.